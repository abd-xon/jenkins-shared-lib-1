def call(Map stageParams) {
  
pipeline {
    environment { 
        registryCredential = "${(stageParams.dockerhubCredential)}"
        registry = "${(stageParams.dockerhubRepo)}"
        dockerImage = '' 
        PROJECT_ID = "${(stageParams.kubeProjectId)}"
        CLUSTER_NAME = "${(stageParams.kubeClustername)}"
        LOCATION = "${(stageParams.kubeLocation)}"
        CREDENTIALS_ID = "${(stageParams.kubeCredentialId)}"
	
    }
    agent any
    stages {
         stage('Building Docker Image') { 
            steps { 
                script { 
                    dockerImage = docker.build registry + ":$BUILD_NUMBER" 
                }
            } 
        }
        stage('Push to Dockerhub') { 
            steps { 
                script { 
                    docker.withRegistry( '', registryCredential ) { 
                    dockerImage.push("latest")
		            dockerImage.push("$BUILD_NUMBER") 
                    }
                } 
            }
        }
        stage('Deploy to GKE-mysql') {
            steps{
                step([
                $class: 'KubernetesEngineBuilder', 
                projectId: env.PROJECT_ID, 
                clusterName: env.CLUSTER_NAME, 
                location: env.LOCATION, 
                manifestPattern: 'notejam-kube/mysql-deploy.yaml', 
                credentialsId: env.CREDENTIALS_ID, 
                verifyDeployments: true
                ])
            }
        }  
	    stage('Deploy to GKE-notejam') {
            steps{
                step([
                $class: 'KubernetesEngineBuilder', 
                projectId: env.PROJECT_ID, 
                clusterName: env.CLUSTER_NAME, 
                location: env.LOCATION, 
                manifestPattern: 'notejam-kube/notejam-deploy.yaml', 
                credentialsId: env.CREDENTIALS_ID, 
                verifyDeployments: true
                ])
            }
        }
    }
}

}