def call(Map stageParams) {
  
pipeline {
    agent any
    stages {
        stage('Git Checkout') {
            steps {
                    checkout([
                    $class: 'GitSCM',
                    branches: [[name:  stageParams.branch ]],
                    userRemoteConfigs: [[ credentialsId: stageParams.credentialsId, url: stageParams.url ]]
                    ])
                  }
            }
        }
    }
}
